<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Size;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Throwable;

class ProductController extends Controller
{
  public function index(){
    return Inertia::render('Products/List',[
        'products' => Product::with(['categories','brand','sizes'])->get(),
        'categories' => Category::all(),
        'brands' => Brand::all(),
        'sizes' => Size::all(),
    ]);
  }

  public function create(){
    return Inertia::render('Products/New');
  }

  public function store(Request $request){
    try {
      if($request->file('file')) {
        $xmlString = file_get_contents($request->file('file'));
        $xmlObject = simplexml_load_string($xmlString);

        if(count($xmlObject) > 0) {
          foreach ($xmlObject as $xmlProd){
            // categories
            $prodCats = explode('|',$xmlProd->Category_ID);
            $prodCatsname = explode('|',$xmlProd->Category);
            foreach ($prodCats as $key => $prodCat){
              $checkCat = Category::find($prodCat);
              if(!$checkCat) {
                $checkCat = new Category;
                $checkCat->id = $prodCat;
              }
              $checkCat->name = $prodCatsname[$key];
              $checkCat->save();
            }

            // Size
            $prodSizes = explode('|',$xmlProd->Size);
            $sizesArray = [];
            foreach ($prodSizes as $key => $prodSize){
              if($prodSize != ''){
                $checkSize = Size::where('name','like',$prodSize)->first();
                if(!$checkSize) $checkSize = new Size;
                $checkSize->name = $prodSizes[$key];
                $checkSize->save();
                $sizesArray[] = $checkSize->id;
              }
            }

            // Brand
            $checkBrand = Brand::where('name','like',$xmlProd->Brand)->first();
            if(!$checkBrand) $checkBrand = new Brand;
            $checkBrand->name = $xmlProd->Brand;
            $checkBrand->save();

            // products
            $check = Product::find($xmlProd->Product_ID);
            if(!$check) {
              $check = new Product;
              // except for Product_ID, Category_ID, and Retail_Price:
              $check->id = $xmlProd->Product_ID;
              $check->retail_price = $xmlProd->Retail_Price;
            }
            $locked = ($check->locked) ? explode('|',$check->locked) : [];
            if(!in_array('brand_id',$locked)) $check->brand_id = $checkBrand->id;
            if(!in_array('sku',$locked)) $check->sku = $xmlProd->SKU;
            if(!in_array('name',$locked)) $check->name = $xmlProd->Name;
            if(!in_array('price',$locked)) $check->price = $xmlProd->Price;
            if(!in_array('description',$locked)) $check->description = $xmlProd->Description;
            if(!in_array('rating_avg',$locked)) $check->rating_avg = $xmlProd->Rating_Avg;
            if(!in_array('rating_count',$locked)) $check->rating_count = $xmlProd->Rating_Count;
            if(!in_array('inventory_count',$locked)) $check->inventory_count = $xmlProd->Inventory_Count;
            if(!in_array('url',$locked)) $check->url = $xmlProd->Product_URL;
            if(!in_array('thumbnail',$locked)) $check->thumbnail = $xmlProd->Thumbnail_URL;
            if(!in_array('occassion',$locked)) $check->occassion = $xmlProd->Occassion;
            if(!in_array('season',$locked)) $check->season = $xmlProd->Season;
            if(!in_array('season',$locked)) $check->season = $xmlProd->Season;
            if(!in_array('badges',$locked)) $check->badges = $xmlProd->Badges;
            $check->save();
            // check and attach categories, sizes
            $check->categories()->sync($prodCats);
            if(!in_array('badges',$locked)) $check->sizes()->sync($sizesArray);
          }
        }
      }
    } catch (Throwable $e) {
      return redirect()
          ->route('products.create');
    }
    return redirect()->route('products.index');
  }

  public function show(){
    return Inertia::render('Products/Show');
  }

  public function update(Request $request, Product $product){
    $product->updated_at = Date('Y-m-d H:i:s');
    $locked = ($product->locked) ? explode('|',$product->locked) : [];
    if($product->name != $request->input('name')){
      if(!in_array('name',$locked)) $locked[] = 'name';
      $product->name = $request->input('name');
    }
    if($product->sku != $request->input('sku')){
      if(!in_array('sku',$locked)) $locked[] = 'sku';
      $product->sku = $request->input('sku');
    }
    if($product->url != $request->input('url')){
      if(!in_array('url',$locked)) $locked[] = 'url';
      $product->url = $request->input('url');
    }
    if($product->thumbnail != $request->input('thumbnail')){
      if(!in_array('thumbnail',$locked)) $locked[] = 'thumbnail';
      $product->thumbnail = $request->input('thumbnail');
    }
    if($product->occassion != $request->input('occassion')){
      if(!in_array('occassion',$locked)) $locked[] = 'occassion';
      $product->occassion = $request->input('occassion');
    }
    if($product->season != $request->input('season')){
      if(!in_array('season',$locked)) $locked[] = 'season';
      $product->season = $request->input('season');
    }
    if($product->badges != $request->input('badges')){
      if(!in_array('badges',$locked)) $locked[] = 'badges';
      $product->badges = $request->input('badges');
    }
    if($product->price != $request->input('price')){
      if(!in_array('price',$locked)) $locked[] = 'price';
      $product->price = $request->input('price');
    }
    if($product->description != $request->input('description')){
      if(!in_array('description',$locked)) $locked[] = 'description';
      $product->description = $request->input('description');
    }
    if($product->brand_id != $request->input('brand_id')){
      if(!in_array('brand_id',$locked)) $locked[] = 'brand_id';
      $product->brand_id = $request->input('brand_id');
    }
    if($product->rating_avg != $request->input('rating_avg')){
      if(!in_array('rating_avg',$locked)) $locked[] = 'rating_avg';
      $product->rating_avg = $request->input('rating_avg');
    }
    if($product->rating_count != $request->input('rating_count')){
      if(!in_array('rating_count',$locked)) $locked[] = 'rating_count';
      $product->rating_count = $request->input('rating_count');
    }
    if($product->inventory_count != $request->input('inventory_count')){
      if(!in_array('inventory_count',$locked)) $locked[] = 'inventory_count';
      $product->inventory_count = $request->input('inventory_count');
    }
    $syncResult = $product->sizes()->sync($request->input('sizes'));
    if((!!$syncResult["attached"] || !!$syncResult["detached"] || !!$syncResult["updated"]) && !in_array('sizes',$locked)) $locked[] = 'sizes';
    $product->locked = implode('|',$locked);
    $product->save();
    return redirect()->route('products.index');
  }
}
