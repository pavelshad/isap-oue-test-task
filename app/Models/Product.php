<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  use HasFactory;

  protected $table = 'products';
  protected $fillable = [
      'sku',
      'name',
      'price',
      'url',
      'thumbnail',
      'occassion',
      'season',
      'badges',
      'shoe_size',
      'pants_size',
      'retail_price',
      'description',
      'brand_id',
      'rating_avg',
      'rating_count',
      'inventory_count',
  ];

  public function brand() {
    return $this->belongsTo(Brand::class,'brand_id');
  }
  public function sizes() {
    return $this->belongsToMany(Size::class,'size_products','size_id','product_id')
                ->withTimestamps();
  }
  public function categories() {
    return $this->belongsToMany(Category::class,'category_products','category_id','product_id')
                ->withTimestamps();
  }
}
