<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

___

## Start project

```
composer create-project laravel/laravel example-app
composer require laravel/breeze

php artisan breeze:install

php artisan key:generate

npm install
npm run build

php artisan migrate

npm run build
//php artisan inertia:start-ssr
```

TailWindCSS postcss

```angular2html
npm install -D postcss-nesting
```

## Errors

if cache error (no cache folder)

create these folders under **storage/framework:**
* sessions
* views
* cache
    * data
___
fix file permissions

```bash
sudo find storage bootstrap/cache -type f -exec chmod g+w {} \;
sudo find storage bootstrap/cache -type d -exec chmod g+ws {} \;
sudo chmod -R guo+w storage
```

