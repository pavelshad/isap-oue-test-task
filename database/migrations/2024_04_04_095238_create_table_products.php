<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  public function up(): void
  {
    if (Schema::hasTable('products')) return;
    Schema::create('products', function (Blueprint $table) {
      $table->id();
      $table->string('sku');
      $table->string('name');
      $table->string('url')->nullable();
      $table->string('thumbnail')->nullable();
      $table->string('occassion')->nullable();
      $table->string('season')->nullable();
      $table->string('badges')->nullable();
      $table->string('shoe_size')->nullable();
      $table->string('pants_size')->nullable();
      $table->float('price')->nullable();
      $table->float('retail_price')->nullable();
      $table->longText('description')->nullable();
      $table->string('brand_id')->nullable();
      $table->float('rating_avg')->nullable();
      $table->integer('rating_count')->default(0);
      $table->integer('inventory_count')->default(0);
      $table->string('locked')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down(): void
  {
    Schema::dropIfExists('products');
  }
};
